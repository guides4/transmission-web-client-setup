# Transmission Web Client with Web Server Guide
This should help you setup a transmission web client with a web server running the transmission daemon. I am using a raspberry pi as my self hosted web server.

Log in as root
```
$ sudo su
```

Stop the transmission daemon:
**Important step: Stopping the transmission daemon will allow you to modify the transmission configuration file and to make the configuration changes to take effect after re-enabling it.**
```
# systemctl stop transmission-daemon
```

Open the transmission-daemon config with a text editor (vim, nano, etc)
```
# vim /var/lib/transmission-daemon/info/settings.json
```

Modify it to allow addresses in the LAN
```
"rpc-username": "transmission",
"rpc-whitelist": "127.0.0.1,192.168.*.*",
"rpc-whitelist-enabled": true,
```

Add the user to the debian-transmission group (probably optional)
```
# usermod -a -G debian-transmission yourusername
```

Start the transmission daemon
```
# systemctl start transmission-daemon
```

Go to the IP address of the server with 9091 port specified. For example, type this on the web browser: 192.168.1.12:9091.

The transmission web client should appear:

<figure>
    <img src="image/web-client.webp" alt="web-client">
    <figcaption>
        <a href="https://transmissionbt.com/images/screenshots/Clutch-Large.jpg">Image Source</a>
    </figcaption>
</figure><br>

Resources:
* https://linuxconfig.org/how-to-set-up-transmission-daemon-on-a-raspberry-pi-and-control-it-via-web-interfacea
* https://www.youtube.com/watch?v=raN2QrGOu04
